﻿using Smart.IK.Modal.ViewModals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smart.IK.Data.SmartIKDataConnection;

namespace Smart.IK.Controllers
{
    public class IKPersonellistController : Controller
    {
        // GET: IKPersonellist
        public ActionResult Personellist()
        {

            using (SmartIKEntities entity = new SmartIKEntities())
            {

                var personelList = (from tablo in entity.ik_personel
                                    join kb in entity.ik_kisisel_bilgiler on tablo.ik_kisisel_bilgiler_id equals kb.id
                                    join un in entity.ik_personel_unvan on tablo.id equals un.personel_id
                                    join unlook in entity.ik_lookup_unvan on un.unvan_id equals unlook.id
                                    join dp in entity.ik_departman on unlook.departman_id equals dp.id
                                   
                                    select new PersonelListingViewModal
                                    {
                                        personel_kimlik = tablo.personel_kimlik,
                                        isim = kb.ad,
                                        soyisim = kb.soyad,
                                        unvan = unlook.unvan_adi,
                                        departman = dp.departman_adi

                                    }).ToList();
            

                return View(personelList);
            }
        }
    }
}