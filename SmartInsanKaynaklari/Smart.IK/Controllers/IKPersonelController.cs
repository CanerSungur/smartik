﻿using Smart.IK.Data.SmartIKDataConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Smart.IK.Modal.ViewModals;
using System.Reflection;

namespace Smart.IK.Controllers
{
    public class IKPersonelController : Controller
    {

        // GET: IKPersonel
        public ActionResult Personel()
        {
            return View();
        }

        public ActionResult PersonelSave(PersonelInsertViewModal modal)
        {
            using (SmartIKEntities ent = new SmartIKEntities())
            {


                var kisiselbilgiler = new ik_kisisel_bilgiler();
                var egitim_yetenek = new ik_egitim_yetenek_bilgiler();
                var tecrube_referans = new ik_tecrube_referans();
                var personel_giris_cikis = new ik_personel_giris_cikis();
                var personel_maas = new ik_maas();
                var evlilik_durumu = new ik_evlilik_durumu();
                var engel_durumu = new ik_engel_durumu();



                #region Kişisel Bilgiler
              

                kisiselbilgiler.ad = modal.ad;
                kisiselbilgiler.soyad = modal.soyad;
                kisiselbilgiler.tc = modal.tcKimlikNo.ToString();
                kisiselbilgiler.cep_tel = modal.cep_tel;
                kisiselbilgiler.ev_tel = modal.ev_tel;
                kisiselbilgiler.dogum_tarihi = modal.dogum_tarihi;
                kisiselbilgiler.dogum_yeri = modal.dogum_yeri;
                kisiselbilgiler.e_posta = modal.e_posta;
                kisiselbilgiler.cocuk_sayisi = modal.cocuk_sayisi;
                //evlilik_durumu.durum = modal.evlilik_durum; //modal dan enum değeri dönüyor ? if/swicht içerinden convert gerekli ?
                kisiselbilgiler.ehliyet_sinifi = modal.ehliyet; 
                //engel_durumu.engel_durumu = modal.engel_seviyesi; //modal dan enum değeri dönüyor ? if/swicht içerinden convert gerekli ?
                kisiselbilgiler.sabika_durumu = modal.sabika_durumu;
                kisiselbilgiler.seyahat_edebilir = modal.seyahat_edebilirlik;


                #endregion


                #region Eğitim Yetenek Bilgileri

                egitim_yetenek.lise_okul = modal.lise_okul;
                egitim_yetenek.lise_tarih = modal.lise_tarih;
                egitim_yetenek.lise_derece = modal.lise_derece;

                egitim_yetenek.onlisans_okul = modal.onlisans_okul;
                egitim_yetenek.onlisans_tarih = modal.onlisans_tarih;
                egitim_yetenek.onlisans_bolum = modal.onlisans_bolum;
                egitim_yetenek.onlisans_derece = modal.onlisans_derece;

                egitim_yetenek.lisans_okul = modal.lisans_okul;
                egitim_yetenek.lisans_tarih = modal.lisans_tarih;
                egitim_yetenek.lisans_bolum = modal.lisans_bolum;
                egitim_yetenek.lisans_derece = modal.lisans_derece;

                egitim_yetenek.yuksek_lisans_okul = modal.yukseklisans_okul;
                egitim_yetenek.yuksek_lisans_tarih = modal.yukseklisans_tarih;
                egitim_yetenek.yuksek_lisans_bolum = modal.yukseklisans_bolum;
                egitim_yetenek.yuksek_lisans_derece = modal.yukseklisans_derece;

                egitim_yetenek.doktora_okul = modal.doktora_okul;
                egitim_yetenek.doktora_bolum = modal.doktora_bolum;
                egitim_yetenek.doktora_tarih = modal.doktora_tarih;


                #endregion


                #region Tecrübe Referans Bilgileri

                tecrube_referans.son_sirket_adi = modal.sirket_adi;
                tecrube_referans.son_sirket_departman = modal.sirket_departman;
                tecrube_referans.son_sirket_baslangic = modal.sirket_baslangic_tarihi;
                tecrube_referans.son_sirket_bitis = modal.sirket_cikis_tarihi;
                tecrube_referans.ayrilma_nedeni = modal.ayrilma_nedeni;
                tecrube_referans.ref1_adsoyad = modal.ref1_adsoyad;
                tecrube_referans.ref1_tel = modal.ref1_tel;
                tecrube_referans.ref2_adsoyad = modal.ref2_adsoyad;
                tecrube_referans.ref2_tel = modal.ref2_tel;

                #endregion


                #region Personel Giriş Çıkış

                personel_giris_cikis.is_giris = DateTime.Now;
                personel_giris_cikis.is_cikis = DateTime.MaxValue;

                #endregion


                #region Maaş

                personel_maas.miktar = modal.maas;


                #endregion


                #region Engel Durumu

                //engel_durumu.engel_durumu = modal.engel_seviyesi;  // Convert lazım


                #endregion


                #region Evlilik Durumu

                //evlilik_durumu.durum = modal.evlilik_durum;  // Convert lazım

                #endregion




                ent.ik_kisisel_bilgiler.Add(kisiselbilgiler);
                ent.ik_egitim_yetenek_bilgiler.Add(egitim_yetenek);
                ent.ik_tecrube_referans.Add(tecrube_referans);
                ent.ik_personel_giris_cikis.Add(personel_giris_cikis);
                ent.ik_maas.Add(personel_maas);
                ent.ik_engel_durumu.Add(engel_durumu);
                ent.ik_evlilik_durumu.Add(evlilik_durumu);
                ent.SaveChanges();

                return View();
            }
        }
    }
}