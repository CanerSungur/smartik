﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Smart.IK.Startup))]
namespace Smart.IK
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
