﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smart.IK.Modal.ViewModals
{
    public class PersonelListingViewModal
    {
        public string personel_kimlik { get; set; }
        public string isim { get; set; }
        public string soyisim { get; set; }
        public string unvan { get; set; }
        public string departman { get; set; }
    }
}
