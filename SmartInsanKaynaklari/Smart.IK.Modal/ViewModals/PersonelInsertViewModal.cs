﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Smart.IK.Modal.ViewModals
{
    public class PersonelInsertViewModal
    {

        #region Kişisel Bilgiler 

        public String ad { get; set; }
        public String soyad { get; set; }
        public int tcKimlikNo { get; set; }
        public String ev_tel { get; set; }
        public String cep_tel { get; set; }
        public String kan_grubu { get; set; }
        public String dogum_yeri { get; set; }
        public DateTime dogum_tarihi { get; set; }
        public String ev_adresi { get; set; }
        public String e_posta { get; set; }
        public String ehliyet { get; set; }
        public int evlilik_durum { get; set; }
        public int cocuk_sayisi { get; set; }
        public int engel_seviyesi { get; set; }
        public bool seyahat_edebilirlik { get; set; }
        public bool sabika_durumu { get; set; }

        #endregion

        #region Eğitim Bilgileri

        /*
         id
lise_okul
lise_tarih
lise_derece
onlisans_okul
onlisans_bolum
onlisans_derece
onlisans_tarih
lisans_okul
lisans_bolum
lisans_tarih
lisans_derece
yuksek_lisans_okul
yuksek_lisans_bolum
yuksek_lisans_tarih
yuksek_lisans_derece
doktora_okul
doktora_bolum
doktora_tarih
yabanci_dil_id

         */

        public String lise_okul { get; set; }
        public DateTime lise_tarih { get; set; }
        public Double lise_derece { get; set; }

        public String onlisans_okul { get; set; }
        public DateTime onlisans_tarih { get; set; }
        public Double onlisans_derece { get; set; }
        public String onlisans_bolum { get; set; }

        public String lisans_okul { get; set; }
        public DateTime lisans_tarih { get; set; }
        public Double lisans_derece { get; set; }
        public String lisans_bolum { get; set; }

        public String yukseklisans_okul { get; set; }
        public DateTime yukseklisans_tarih { get; set; }
        public String yukseklisans_derece { get; set; }
        public String yukseklisans_bolum { get; set; }

        public String doktora_okul { get; set; }
        public DateTime doktora_tarih { get; set; }
        public String doktora_bolum { get; set; }

        public String dil_adi { get; set; }
        public String dil_seviye { get; set; }


        #endregion

        #region Tecrübe Referans


        public String sirket_adi { get; set; }
        public String sirket_departman { get; set; }
        public DateTime sirket_baslangic_tarihi { get; set; }
        public DateTime sirket_cikis_tarihi { get; set; }
        public String ayrilma_nedeni { get; set; }
        public String ref1_adsoyad { get; set; }
        public String ref1_tel { get; set; }
        public String ref2_adsoyad { get; set; }
        public String ref2_tel { get; set; }


        #endregion

        #region İş Giriş Çıkış Tarihleri

        public DateTime ise_giris_tarihi { get; set; }
        public DateTime isten_cikis_tarihi { get; set; }

        #endregion

        #region Maaş

        public Decimal maas { get; set; }

        #endregion

    }

    #region Enums

    public enum Evlilik
    {
        Bekar,
        EvliEşiÇalışmıyor,
        EvliEşiÇalışıyor
    }

    public enum Ehliyet
    {
        M,
        A1,
        A2,
        A,
        B1,
        B,
        BE,
        C1,
        C1E,
        C,
        CE,
        D1,
        D1E,
        D,
        DE,
        F,
        G,
    }

    public enum EngelDurumu
    {
        EngeliYok,
        EngelSeviyesi_1,
        EngelSeviyesi_2,
        EngelSeviyesi_3,
    }

    public enum SabıkaDurumu
    {
        Var,
        Yok
    }

    public enum SeyahatDurumu
    {
        Uygun,
        UygunDeğil
    }

    public enum CocukSayisi
    {
        Yok,
        Bir = 1,
        İki = 2,
        Üç = 3,
        Dört = 4,
        Beş_Ve_Üzeri = 5
    }

    public enum Yabancidil_Seviyesi
    {
        Başlangıç,
        Orta,
        İyi,
        Çok_İyi
    }

    public enum Departman
    {
        İnsan_Kaynakları,
        Muhasebe,
        Stok,
        Raporlama,
    }

    public enum Unvan
    {
        Mühendis,
        İşçi,
        Mali_Müşavir,
        Şef,
        Müdür,
        Müdür_Yardımcısı,
    }

    public enum Yetki
    {
        Yetki_1,
        Yetki_2,
        Yetki_3,
        Yetki_4,
    }

    public enum ParaBirimi
    {
        TL,
        Dolar,
        Euro
    }
    #endregion


}
